FROM tiangolo/uwsgi-nginx-flask:python3.6

COPY panchayat /app/panchayat
COPY main.py /app
COPY uwsgi.ini /app

ENV STATIC_URL /static
ENV STATIC_PATH /app/panchayat/static

RUN pip install -r panchayat/requirements.txt