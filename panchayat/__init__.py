"""
__init__.py file for panchayat
Install the app, as well as loads the config.
"""

import os

from flask import Flask
import yaml
from panchayat.vdb import VDB


def create_app(test_config=None):
    """
    Create the app instance
    """
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.urandom(
            16
        ),  # randomize secret key on app start to force relogin after app start
        DATABASE=os.path.join(app.instance_path, 'panchayat.yaml'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed instance
        app.config.from_mapping(test_config)

    app.config.from_mapping(
        ROOT_URL=
        f"https://{app.config['SLUG']}.{app.config['EXTERNAL_DOMAIN']}",
        ROOT_EMAIL=f"{app.config['SLUG']}@{app.config['EXTERNAL_DOMAIN']}")
    #ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # load database
    db_path = app.config['DATABASE']
    with open(db_path, 'r') as f_in:
        vdb = yaml.load(f_in, Loader=yaml.FullLoader)
        if vdb is not None:
            vdb.outfile = db_path

    app.config.from_mapping(VDB=vdb)

    with app.app_context():
        from . import db  # pylint: disable=import-outside-toplevel
        db.init_app(app)

        from . import auth  #pylint: disable=import-outside-toplevel
        app.register_blueprint(auth.BP)

        from . import blog  #pylint: disable=import-outside-toplevel
        app.register_blueprint(blog.BP)
        app.add_url_rule('/', endpoint='index')

        from . import user  #pylint: disable=import-outside-toplevel
        app.register_blueprint(user.BP)

    return app
