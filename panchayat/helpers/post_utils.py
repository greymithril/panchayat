"""
Helper functions for reply
"""

import ssl
import urllib.request

import certifi
from bs4 import BeautifulSoup  # type: ignore
from flask import flash, g, redirect, render_template, request, url_for

from panchayat.db import get_db
from panchayat.helpers.mail_utils import broadcast_post
from panchayat.vdb import LinkPost, TextPost


def reply(parent, is_url):  #pylint: disable=too-many-branches
    """
    Reply helper function
    Called from reply endpoint for every submission from website
    """
    suggested_title = None
    if request.method == 'POST':
        if 'title_button' in request.form:
            url = request.form['body']
            if not url or not (url.startswith('http://')
                               or url.startswith('https://')):
                flash("INSUFFICIENT DATA FOR MEANINGFUL ANSWER")
            else:
                try:
                    req = urllib.request.Request(
                        url,
                        data=None,
                        headers={
                            'User-Agent':
                            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)\
                            AppleWebKit/537.36 (KHTML, like Gecko)\
                            Chrome/35.0.1916.47 Safari/537.36'
                        })
                    soup = BeautifulSoup(
                        urllib.request.urlopen(
                            req,
                            context=ssl.create_default_context(
                                cafile=certifi.where())))
                    suggested_title = soup.title.string

                except:  # pylint: disable=bare-except
                    flash("INSUFFICIENT DATA FOR MEANINGFUL ANSWER")

        else:
            if parent is None or is_url:
                title = request.form['title']
                if not title:
                    error = 'Title is required'
            else:
                title = ''
            body = request.form['body']
            error = None

            vdb = get_db()
            if error is not None:
                flash(error)
            else:
                if is_url:
                    PostCls = LinkPost
                else:
                    PostCls = TextPost

                post = PostCls(author=g.user,
                               title=title,
                               body=body,
                               parent=parent)

                vdb.posts.insert(post)
                vdb.commit()
                broadcast_post(post)

                referrer = request.args.get('referrer', 'blog.index')
                target_url = url_for(referrer)
                if parent:
                    target_url += '#' + str(parent.post_id)
                return redirect(target_url)

    return render_template('blog/reply.html',
                           parent=parent,
                           is_url=is_url,
                           suggested_title=suggested_title)
