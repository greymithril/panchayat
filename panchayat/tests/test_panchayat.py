"""
Contains all tests for panchayat functionality
"""

import os
import re
import tempfile
from typing import List, Tuple, Dict
import pytest  # type: ignore
from werkzeug.security import check_password_hash
from panchayat.vdb import TextPost, LinkPost, User
from panchayat import create_app
from panchayat.db import init_db

# pylint: disable=line-too-long, too-few-public-methods, redefined-outer-name, unused-argument


class MailSimulator:
    """
    Class to simulate email functionality
    """
    sent_emails: List['Email'] = []

    class Email:
        """
        Class to represent a standard email object
        """
        def __init__(self,
                     from_address,
                     to_address,
                     subject,
                     body,
                     reply_to=None):
            # pylint: disable=too-many-arguments
            self.from_address = from_address
            self.to_address = to_address
            self.subject = subject
            self.body = body
            self.reply_to = reply_to

        def __str__(self):
            return f'To: {self.to_address}\n'\
                   f'Subject: {self.subject}\n'\
                   f'Body: {self.body}\n'\
                   f'Reply_to: {self.reply_to}'\

    class Mailgun:
        """
        Class to simulate Mailgun functionality
        """
        class MailgunRoute:
            """
            Class to represent a forward route in Mailgun
            """
            match_recipient = r'^panchayattest\+?([0-9]*)@haletic.com$'
            forward_address = r'/submit?parent=\1'

        @classmethod
        def webhook(cls, email) -> Tuple[str, Dict]:
            """
            Return webhook that would have been trigerred by Mailgun for an email
            """
            url = re.sub(cls.MailgunRoute.match_recipient,
                         cls.MailgunRoute.forward_address, email.to_address)
            form = {
                'From': f'<{email.from_address}>',
                'subject': email.subject,
                'stripped-html': email.body,
                'token': 'DEADBEEF',
                'timestamp': 'DEADBEEF',
                'signature': 'DEADBEEF',
            }

            return url, form

    @staticmethod
    def send_mail_monkeypatch(to_address: str, subject: str, body: str,
                              reply_to: str):
        """
        Monkey patch the send_mail function of panchayat to intercept function calls.
        Overide with this function which appends email to sent_emails
        """
        MailSimulator.sent_emails.append(
            MailSimulator.Email(
                from_address='panchayattest@haletic.com',
                to_address=to_address,
                subject=subject,
                body=body,
                reply_to=reply_to,
            ))

    @staticmethod
    def is_mailgun_monkeypatch(api_key, token, timestamp, signature):  #pylint: disable=unused-argument
        """
        Monkeypatch is_mailgun function of panchayat to bypass mailgun authentication.
        Panchayat only allows post via webhook after authenticating Mailgun with token.
        This allows us to trick Panchayat to submit webhook while testing without auth.
        """
        return True

    @classmethod
    def simulate_mail_receive(cls, email, client):
        """
        Simulate receiving an email which triggers the webhook
        """
        url, data = cls.Mailgun.webhook(email)
        client.post(url, data=data, follow_redirects=True)


@pytest.fixture
def app(monkeypatch):
    """
    Fixture that returns an instance of app
    """
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({
        "TESTING": True,
        "DATABASE": db_path,
        "EXTERNAL_DOMAIN": "haletic.com",
        "SLUG": "panchayattest",
    })

    with app.app_context():
        init_db()
        # monkey patching panchayat send_mail function to intercept emails
        from panchayat.helpers import mail_utils  #pylint: disable=import-outside-toplevel
        from panchayat import auth  #pylint: disable=import-outside-toplevel
        monkeypatch.setattr(mail_utils, "send_mail",
                            MailSimulator.send_mail_monkeypatch)
        monkeypatch.setattr(auth, "is_mailgun",
                            MailSimulator.is_mailgun_monkeypatch)

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def vdb(app):
    """
    Fixture that returns the vdb of the app.
    Can only be called once in the test hierarchy.
    """
    return app.config['VDB']


@pytest.fixture
def client(app):
    """
    Fixtures that returns a test client for the app.
    Client has http get/post methods.
    """
    return app.test_client()


def test_index_before_log_in(client):
    """
    Test get panchayat index page
    """
    rv = client.get('/', follow_redirects=True)
    assert b'<h1>Log In</h1>' in rv.data


def test_register_page(client):
    """
    Test panchayat register page
    """
    rv = client.get('/auth/register')
    assert b'<h1>Register</h1>' in rv.data


def test_user_registration_errors(client):
    """
    Test invalid registration form submits
    """
    data = {"username": "", "password": "test123"}
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'Username is required.' in rv.data

    data = {"username": "test", "password": ""}
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'Password is required.' in rv.data

    data = {"username": "test", "password": "test123"}
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'<h1>Log In</h1>' in rv.data

    data = {"username": "test", "password": "test123"}
    rv = client.post('/auth/register', data=data, follow_redirects=True)
    assert b'User test is already registered' in rv.data


@pytest.fixture
def user_client(client):
    """
    Fixture that returns client containing dummy test user
    """
    data = {"username": "test", "password": "test123"}
    _ = client.post('/auth/register', data=data, follow_redirects=True)

    return client


def test_user_registration_vdb(user_client, vdb):
    """
    Test user registration reflecting in vdb
    """
    assert len(vdb.users) == 1
    test_user = vdb.users.find('test')
    assert test_user.username == 'test'
    assert check_password_hash(test_user.password, 'test123')


def test_log_in(user_client):
    """
    Test invalid and valid login attempts
    """
    data = {"username": "", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Invalid username' in rv.data

    data = {"username": "test", "password": ""}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Invalid password' in rv.data

    data = {"username": "testx", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Incorrect username' in rv.data

    data = {"username": "test", "password": "test123x"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'Incorrect password' in rv.data

    data = {"username": "test", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'<h1>Posts</h1>' in rv.data


@pytest.fixture
def logged_in_client(user_client):
    """
    Fixture to return client containing a logged in user
    """
    data = {"username": "test", "password": "test123"}
    rv = user_client.post('/auth/', data=data, follow_redirects=True)
    assert b'<li><a href="/user/">test</a></li>' in rv.data
    return user_client


def test_new_post_page(logged_in_client):
    """
    Test get new post page
    """
    rv = logged_in_client.get('/submit-text', follow_redirects=True)
    assert b'<label for="title">Title</label' in rv.data
    assert b'<input name="title" id="title" value="">' in rv.data
    assert b'<label for="body">Body</label>' in rv.data
    assert b'<textarea name="body" id="body"></textarea>' in rv.data

    rv = logged_in_client.get('/submit-url', follow_redirects=True)
    assert b'<label for="title">Title</label' in rv.data
    assert b'<input name="title" id="title" value="">' in rv.data
    assert b'<label for="body">Link URL</label>' in rv.data
    assert b'<input name="body" id="body" value="">' in rv.data


@pytest.fixture
def link_post_client(logged_in_client):
    """
    Fixture to return client containing dummy link post
    """
    data = {"title": "alpha_post_title", "body": "alpha_post_url"}
    _ = logged_in_client.post('/submit-url', data=data, follow_redirects=True)
    return logged_in_client


@pytest.fixture
def text_post_client(logged_in_client):
    """
    Fixture to return client with dummy text client
    """
    data = {"title": "alpha_post_title", "body": "alpha_post_body"}
    _ = logged_in_client.post('/submit-text', data=data, follow_redirects=True)
    return logged_in_client


def test_submit_url(link_post_client, vdb):
    """
    Test submitting link post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 1
    post = vdb.posts.find(1)
    assert isinstance(post, LinkPost)
    assert post.title == "alpha_post_title"
    assert post.body == "alpha_post_url"
    assert post.author.username == "test"


def test_submit_text(text_post_client, vdb):
    """
    Test submitting text post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 1
    post = vdb.posts.find(1)
    assert isinstance(post, TextPost)
    assert post.title == "alpha_post_title"
    assert post.body == "alpha_post_body"
    assert post.author.username == "test"


def test_edit_text_post_page(text_post_client):
    """
    Test http get for editing a page
    """
    rv = text_post_client.get('/posts/1/update?referrer=blog.index',
                              follow_redirects=True)
    assert b'<h1>Edit</h1>' in rv.data
    assert b'<input name="title" id="title"\n           value="alpha_post_title">' in rv.data
    assert b'<textarea name="body" id="body">alpha_post_body</textarea>' in rv.data


def test_edit_link_post_page(link_post_client):
    """
    Test http get for editing a page
    """
    rv = link_post_client.get('/posts/1/update?referrer=blog.index',
                              follow_redirects=True)
    assert b'<h1>Edit</h1>' in rv.data
    assert b'<input name="title" id="title"\n           value="alpha_post_title">' in rv.data
    assert b'<textarea name="body" id="body">alpha_post_url</textarea>' in rv.data


@pytest.fixture
def edit_post_client(text_post_client):
    """
    Fixture returning client with an edited post
    """
    data = {"title": "edited_post_title", "body": "edited_post_body"}
    _ = text_post_client.post('posts/1/update',
                              data=data,
                              follow_redirects=True)
    return text_post_client


@pytest.fixture
def deleted_post_client(text_post_client):
    """
    Fixture returning client with a deleted post
    """
    _ = text_post_client.post('posts/1/delete', follow_redirects=True)
    return text_post_client


def test_edit_post(edit_post_client, vdb):
    """
    Text post edit reflected in VDB
    """
    post = vdb.posts.find(1)
    assert post.title == "edited_post_title"
    assert post.body == "edited_post_body"


def test_delete_post(deleted_post_client, vdb):
    """
    Test post deletion reflected in VDB
    """
    post = vdb.posts.find(1)
    assert post.title == "DELETED"
    assert post.body == "DELETED"


@pytest.fixture
def reply_text_post_client(text_post_client):
    """
    Fixture returning client with a text reply comment
    """
    data = {"body": "reply_post_body", "save_button": "Save"}
    _ = text_post_client.post('/posts/1/reply-text',
                              data=data,
                              follow_redirects=True)
    return text_post_client


def test_reply_post(reply_text_post_client, vdb):
    """
    Text reply post reflecting in VDB
    """
    assert len(vdb.posts.all()) == 2
    parent_post = vdb.posts.find(1)
    reply_post = vdb.posts.find(2)
    assert parent_post.children[0] == reply_post
    assert isinstance(reply_post, TextPost)
    assert reply_post.parent == parent_post


@pytest.fixture
def upvote_client(text_post_client):
    """
    Fixture returning client with an upvoted post
    """
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    return text_post_client


@pytest.fixture
def downvote_client(text_post_client):
    """
    Fixture returning client with a downvoted client
    """
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    return text_post_client


@pytest.fixture
def unvote_client(text_post_client):
    """
    Fixture returning client with multiple upvote/downvote actions
    """
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/upvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    _ = text_post_client.get('/posts/1/downvote', follow_redirects=True)
    return text_post_client


def test_vote(upvote_client, vdb):
    """
    Test upvote.
    """
    user = vdb.users.find("test")
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 1
    assert len(post.downvotes) == 0
    assert user in post.upvotes


def test_downvote(downvote_client, vdb):
    """
    Test downvoting
    """
    user = vdb.users.find("test")
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 0
    assert len(post.downvotes) == 1
    assert user in post.downvotes


def test_unvote(unvote_client, vdb):
    """
    Test idempotence of voting
    """
    post = vdb.posts.find(1)
    assert len(post.upvotes) == 0
    assert len(post.downvotes) == 0


def test_user_page(logged_in_client):
    """
    Test get user page
    """
    rv = logged_in_client.get('/user/', follow_redirects=True)
    assert b'<h3>User Settings</h3>' in rv.data


def test_user_stats(upvote_client, vdb):
    """
    Test user stats
    """
    user = vdb.users.find("test")
    assert vdb.posts.tlp_count(user) == 1
    assert vdb.posts.comment_count(user) == 0
    assert vdb.posts.upvote_count(user) == 1
    assert vdb.posts.downvote_count(user) == 0


@pytest.fixture
def generated_token_client(logged_in_client):
    """
    Fixture returning client with a generated token for dummy user
    """
    _ = logged_in_client.post('/user/token?action=generate',
                              follow_redirects=True)
    return logged_in_client


@pytest.fixture
def destroyed_token_client(generated_token_client):
    """
    Fixture returning client after destroying token for dummy user
    """
    _ = generated_token_client.post('/user/token?action=destroy',
                                    follow_redirects=True)
    return generated_token_client


def test_generate_token(generated_token_client, vdb):
    """
    Test token generation for user
    """
    user = vdb.users.find("test")
    assert user.token is not None


def test_destroy_token(destroyed_token_client, vdb):
    """
    Test token destruction for user
    """
    user = vdb.users.find("test")
    assert user.token == ""


def test_activity_page(logged_in_client):
    """
    Test get activity page
    """
    rv = logged_in_client.get('/activity', follow_redirects=True)
    assert b'<h1>Activity</h1>' in rv.data


def test_rss(text_post_client):
    """
    Text get RSS feed
    """
    rv = text_post_client.get('/feed', follow_redirects=True)
    assert b'<title type="text">Panchayat RSS Feed</title>' in rv.data


class TestEmail:
    """
    Class to encapsulate all email related tests
    """
    @staticmethod
    @pytest.fixture
    def enable_test_email(user_client, vdb):
        """
        Update vdb to add mailgun user, enable email for test user
        """
        test_user = vdb.users.find('test')
        test_user.email = "test_user@email_provider.com"
        test_user.email_updates = True

        vdb.users.append(
            User(
                username='mailgun',
                password='',
                token='mailgun_api_key',
            ))
        vdb.commit()

        yield

        #flush sent emails after test finishes
        MailSimulator.sent_emails = []

    @staticmethod
    def test_link_email_send(enable_test_email, link_post_client):
        """
        Test posting a link sends right email
        """
        assert len(MailSimulator.sent_emails) == 1
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"
        assert sent_email.subject == "alpha_post_title"
        assert '<a href="alpha_post_url">alpha_post_title</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+1@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+1@haletic.com'

    @staticmethod
    def test_text_email_send(enable_test_email, text_post_client):
        """
        Test posting text sends right email
        """
        assert len(MailSimulator.sent_emails) == 1
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"
        assert sent_email.subject == "alpha_post_title"
        assert '<h1>\n            \n                \n                    alpha_post_title\n                \n            \n        </h1>' in sent_email.body
        assert '<p class="body" style="white-space: pre-line; word-wrap: break-word;">alpha_post_body</p>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+1@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+1@haletic.com'

    @staticmethod
    def test_text_comment_email_send(enable_test_email,
                                     reply_text_post_client):
        """
        Test replying to a post sends right email
        """
        assert len(MailSimulator.sent_emails) == 2
        sent_email = MailSimulator.sent_emails[-1]
        assert sent_email.to_address == "test_user@email_provider.com"

        #subject is the tlp post subject to enable threading emails
        assert sent_email.subject == "alpha_post_title"
        assert '<p class="body" style="white-space: pre-line; word-wrap: break-word;">reply_post_body</p>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest+2@haletic.com?subject=alpha_post_title">Reply</a>' in sent_email.body
        assert '<a class="action" href="mailto:panchayattest@haletic.com">New Post</a>' in sent_email.body
        assert sent_email.reply_to == 'panchayattest+2@haletic.com'

    @staticmethod
    @pytest.fixture
    def text_email_receive(user_client, enable_test_email):
        """
        Fixture that simulates receiving dummy new text post email
        Fixture invokes Mailgun simulator and triggers the appropriate webhook
        """
        email = MailSimulator.Email(
            from_address='test_user@email_provider.com',
            to_address="panchayattest@haletic.com",
            subject="alpha_post_title",
            body="alpha_post_body")
        MailSimulator.simulate_mail_receive(email, user_client)

    @staticmethod
    def test_text_email_receive(text_email_receive, app, vdb):
        """
        Test receiving new text post email reflected in VDB
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, TextPost)
        assert post.title == "alpha_post_title"
        assert post.body == "alpha_post_body"

    @staticmethod
    @pytest.fixture
    def link_email_receive(user_client, enable_test_email):
        """
        Fixture that simulates receiving dummy new link post email
        Fixture invokes Mailgun simulator and triggers the appropriate webhook
        """
        email = MailSimulator.Email(
            from_address='test_user@email_provider.com',
            to_address="panchayattest@haletic.com",
            subject="alpha_post_title",
            body="http://www.alpha_post_url.com")
        MailSimulator.simulate_mail_receive(email, user_client)

    @staticmethod
    def test_link_email_receive(link_email_receive, app, vdb):
        """
        Test receiving new link post email reflected in VDB
        """
        assert len(vdb.posts.all()) == 1
        post = vdb.posts.find(1)
        assert isinstance(post, LinkPost)
        assert post.title == "alpha_post_title"
        assert post.body == "http://www.alpha_post_url.com"

    @staticmethod
    @pytest.fixture
    def text_email_comment(link_post_client, enable_test_email):
        """
        Fixture that simulates receiving a reply email
        Fixture invokes Mailgun simulator and triggers the appropriate webhook
        """
        email = MailSimulator.Email(
            from_address='test_user@email_provider.com',
            to_address="panchayattest+1@haletic.com",
            subject="alpha_comment_title",
            body="alpha_comment_body")
        MailSimulator.simulate_mail_receive(email, link_post_client)

    @staticmethod
    def test_text_email_comment(text_email_comment, app, vdb):
        """
        Test receiving reply email reflected in VDB
        """
        assert len(vdb.posts.all()) == 2
        parent = vdb.posts.find(1)
        comment = vdb.posts.find(2)
        assert comment.parent is parent
        assert isinstance(comment, TextPost)
        assert comment.title == "alpha_comment_title"
        assert comment.body == "alpha_comment_body"
