import semantic_version
import click
import sqlite3

def get_code_version(version_file):
    with open(version_file, 'r') as version_file_handle:
        version_string = version_file_handle.read()
    return semantic_version.Version(version_string, partial=True)

class DB:
    def __init__(self, path):
        self.db_path = path
        self.connection = sqlite3.connect(path)

    @property
    def version(self):
        version_string = self.connection.execute(
            "select * from properties where key = 'version'"
        ).fetchone()[1]
        return semantic_version.Version(version_string, partial=True)

    def upgrade(self):
        raise NotImplementedError

@click.command()
@click.option('--sqlite', type=click.Path(exists=True), help="path to database file")
@click.option('--version', type=click.Path(exists=True), help="path to VERSION.TXT")
def main(sqlite, version):
    db = DB(sqlite)
    while db.version.major < get_code_version(version).major:
        db.upgrade()

    if db.version > get_code_version(version):
        raise ValueError("Database version overshot code version")

if __name__ == "__main__":
    main()
