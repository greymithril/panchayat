#!/usr/bin/env python3
'''
#**************************************************************************#
#   Reddit2Panchayat.py                                                    #
#   by                                                                     #
#   Debanjum Singh Solanky                                                 #
#                                                                          #
#   Copyright (C) 2019-2020  Debanjum Singh Solanky                        #
#                                                                          #
#   This program is free software: you can redistribute it and/or modify   #
#   it under the terms of the GNU General Public License as published by   #
#   the Free Software Foundation, either version 3 of the License, or      #
#   (at your option) any later version.                                    #
#                                                                          #
#   This program is distributed in the hope that it will be useful,        #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of         #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
#   GNU General Public License for more details.                           #
#**************************************************************************#

#**************************************************************************#
#   DEPENDENCIES                                                           #
# ------------------------------------------------------------------------ #
#   Language: Python3, Module(s): Praw                                     #
#   pip3 install praw                                                      #
#**************************************************************************#

#**************************************************************************#
#   RUN                                                                    #
# ------------------------------------------------------------------------ #
#   Arguments reddit <username>, <password> to run script                  #
#   <subreddit> to be imported and <format> for markup for local files,    #
#   this can be one of reStructeredText(rst) or (default)Markdown(md)      #
#   ./SubReddit_Importer.py <username> <password> <subreddit> [rst|md]     #
#**************************************************************************#
'''

# Import Modules
import praw
import sqlite3
import sys
from datetime import datetime as dt
from subprocess import check_output
from panchayat import vdb
import yaml

def get_submissions(username, password, subredditname):
    "Authenticate crawler and get all subreddit submissions"

    # create bot
    r = praw.Reddit(client_id="UeTtR68ENV7BkA",
                    client_secret="vn_5EJchafDbAbT9TZvzVzOXXC4",
                    user_agent='subreddit importer',
                    username=username,
                    password=password)

    # create generator to lazy import from subreddit
    subreddit = r.subreddit(subredditname)

    # return all submissions, ordered by newest first
    return subreddit.hot(limit=None)

def import2panchayat(username, password, subredditname, indb_path, outdb_path):
    "Import Subreddit Posts and Comments to Panchayat"

    with open(indb_path, 'r') as f_in:
        indb = yaml.load(f_in, Loader=yaml.FullLoader)
        indb.outfile = outdb_path

    post_map = {}  # posts data list

    # authenticate, and get submissions
    submissions = get_submissions(username, password, subredditname)

    tlp_count = 0
    comment_count = 0

    for tlp in submissions:
        if tlp.author.name == "mdtsinha":
            print("skipping mudit")
            post_map[tlp.id] = None
            continue
        elif tlp.author.name == "nayaks26":
            username = "morrie"
        else:
            username = tlp.author.name
        author = indb.users.find(username)
        if author is None:
            raise RuntimeError

        if tlp.is_self:
            post = vdb.TextPost(
                author=author,
                title=tlp.title,
                body=tlp.selftext,
                upvotes=None,
                downvotes=None,
                created=dt.fromtimestamp(tlp.created_utc),
                parent=None,
                post_id=None,
            )
        else:
            post = vdb.LinkPost(
                author=author,
                title=tlp.title,
                body=tlp.url,
                upvotes=None,
                downvotes=None,
                created=dt.fromtimestamp(tlp.created_utc),
                parent=None,
                post_id=None,
            )
        print(f'inserting tlp {post.author} {post}')
        tlp_count += 1
        indb.posts.insert(post) # insertion sets post id
        post_map[tlp.id] = post # map to link child to parent

        for comment in tlp.comments.list():
            if comment.author.name == "mdtsinha":
                print("skipping mudit")
                post_map[comment.id] = None
                continue
            elif comment.author.name == "nayaks26":
                username = "morrie"
            elif comment.author.name == "advait91":
                username = "advaitt"
            else:
                username = comment.author.name
            author = indb.users.find(username)
            if author is None:
                raise RuntimeError
            try:
                parent = post_map[comment.parent_id[3:]]
            except KeyError:
                import pudb; pudb.set_trace()
            if parent is None:
                print("skipping mudit")
                post_map[comment.id] = None

            post = vdb.TextPost(
                author=author,
                title='',
                body=comment.body,
                upvotes=None,
                downvotes=None,
                created=dt.fromtimestamp(comment.created_utc),
                parent=parent,
                post_id=None,
            )

            comment_count += 1
            indb.posts.insert(post) # insertion sets post id
            post_map[comment.id] = post # map to link child to parent

    indb.commit()
    print("[+] Import Completed!")
    print(f'tlps {tlp_count} comments {comment_count}')

author_name_to_id_map = { u'alb_d': 1, u'nayaks26': 2, u'hoperyto': 3, u'therollingdungbeetle': 4, u'dmaanitm': 5, u'sriharis304': 6, u'mdtsinha': 7 }

def format_panchayat_sql(post):
    post_time = dt.fromtimestamp(float(post.created_utc)).strftime("%Y-%m-%d %H:%M:%S")
    post_body = post.selftext if post.selftext else post.url
    is_url = 1 if post.url else 0
    author_id = author_name_to_id_map[post.author.name]

    print("    [*] {}({}), {}, {}, {}, {}".format(post.author.name, author_id, post_time, post.title.encode('utf-8'), post_body.encode('utf-8'), is_url))
                                 
    return (author_id, post_time, post.title, post_body, is_url)


def import2md(username, password, subredditname):
    "Import Subreddit Posts and Comments to Markdown"

    # open overview page to export subreddit data too
    overview = open("{0}.md".format(subredditname), 'w')

    # authenticate, and get submissions
    submissions = get_submissions(username, password, subredditname)

    # for each submission to subreddit
    print("[+] Importing Posts")
    for post in submissions:
        # extract title
        print("  [+] "+post.title)
        if post.url:
            message = "["+post.title+"]("+post.url+")\n"  # md format of url in title
        else:
            message = post.title + "\n"                   # rst format of no url in title
        message += '=' * len(message)                     # add markup for header

        # extract post author and data created
        message += "\n[" + post.id + "](" + post.id + ".md)" + " by **" + post.author.name + "**"
        message += dt.fromtimestamp(float(post.created_utc)).strftime(" at *%H:%M:%S* on *%d-%m-%Y*\n\n")

        # write title, post_id, author, creation_date to overview file containing all post summaries
        overview.write(message)

        # extract post body, if it exists
        if post.selftext:
            message += "\n    " + post.selftext
        message += '\n\n\n'

        # extract all comments and their authors
        for comment in post.comments:
            if hasattr(comment, 'author'):
                if hasattr(comment.author, 'name'):
                    message += "    + **" + comment.author.name + "**"
                    message += dt.fromtimestamp(float(comment.created_utc)).strftime(" at *%H:%M:%S* on *%d-%m-%Y*:\n\n")
                    if comment.body:
                        message += "     *" + comment.body + "*\n\n"
                    else:
                        message += "\n\n\n"

            elif comment.body:
                message += "    + *" + "[unknown]" + "*:\n"
                message += "     " + comment.body + "\n\n"

        # write post with comments to separate file
        postfile = open("{0}.md".format(post.id), 'w')
        postfile.write(message)
        postfile.close()

    print("[+] Import Completed!")
    overview.close()


def import2rst(username, password, subredditname):
    "Import Subreddit Posts and Comments to ReStructuredText"

    # open overview page to export subreddit data too
    overview = open("{0}.rst".format(subredditname), 'w')

    # authenticate, and get submissions
    submissions = get_submissions(username, password, subredditname)

    # for each submission to subreddit
    print("[+] Importing Posts")
    for post in submissions:
        # extract title
        print("  [+] "+post.title)
        if post.url:
            message = "`"+post.title+" <"+post.url+">`_\n"  # rst format if url in title
        else:
            message = post.title + "\n"                     # rst format if no url in title
        message += '=' * len(message)                       # add markup for header

        # extract post author and data created
        message += "\n`"+post.id+" <"+post.id+".rst>`_" + " by **" + post.author.name + "**"
        message += dt.fromtimestamp(float(post.created_utc)).strftime(" at *%H:%M:%S* on *%d-%m-%Y*\n\n")

        # write to landing/overview page to link to all imported subreddit posts
        overview.write(message)

        # extract post body, if it exists
        if post.selftext:
            message += "\n    " + post.selftext
        message += '\n\n\n'

        # extract all comments and their authors
        for comment in post.comments:
            if hasattr(comment, 'author'):
                if hasattr(comment.author, 'name'):
                    message += "    + **" + comment.author.name + "**"
                    message += dt.fromtimestamp(float(comment.created_utc)).strftime(" at *%H:%M:%S* on *%d-%m-%Y*:\n\n")
                    if comment.body:
                        message += "      *" + comment.body + "*\n\n"
                    else:
                        message += "\n\n\n"

            elif comment.body:
                message += "    + *" + "[unknown]" + "*:\n"
                message += "      " + comment.body + "\n\n"

        # write post with comments to separate file
        postfile = open("{0}.rst".format(post.id), 'w')
        postfile.write(message)
        postfile.close()

    print("[+] Import Completed!")
    overview.close()


def import2org(username, password, subredditname, title, outfile):
    "Import Subreddit Posts and Comments to Org-Mode"

    # authenticate, and get submissions
    submissions = get_submissions(username, password, subredditname)

    # add org-mode configuration for top of org file
    message = "#+SETUPFILE: theme/setup/theme-darksun-local.setup\n#+TITLE: {}\n#+OPTIONS: \\n:t\n".format(title)

    # for each submission to subreddit
    print("[+] Importing Posts")
    for post in submissions:
        # extract title
        print("  [+] "+post.title)

        # Create Post Heading with Timestamp and Author Name
        message += format_org_heading(post)

        # extract the whole comment tree and their authors
        post.comments.replace_more(limit=None)
        for comment in post.comments:
            message += create_message(comment, 0)
            with open("{0}.org".format(outfile), 'w') as overview:
                overview.write(message)

    # add org-mode html export configuration for end of file
    config = "\n* COMMENT HTML Export Configuration\n# Local Variables:\n# org-html-mathjax-template: \"\"\n# org-html-inline-images: nil\n# End:\n"
    message += "{}".format(config)

    # write subreddit data to org mode file
    with open("{0}.org".format(outfile), 'w') as overview:
        overview.write(message)

    print("[+] Import Completed!")


def format_org_heading(post):
    post_time = dt.fromtimestamp(float(post.created_utc)).strftime("[%Y-%m-%d %a %H:%M]")
    post_title = post.title.replace("[", "{").replace("]", "}")

    # transform reddit formatting for org rendering
    if post.selftext:
       post_body = "-" + post.selftext[1:] if len(post.selftext) > 0 and post.selftext[0] == "*" else post.selftext
       post_body = post_body.replace("\n*", "\n-")
       post_body = "{}\n".format(post_body)
    else:
        post_body = ""

    if post.url:
        message = "* {} [[{}][{}]] :@{}:\n{}".format(post_time, post.url, post_title, post.author.name, post_body)  # org format if url in title
    else:
        message = "* {} {} :@{}:\n{}".format(post_time, post.title, post.author.name, post_body)  # org format if no url in title
    return message

    
def create_message(comment, level):
    '''Depth first traversal into comment tree'''
    if len(comment.replies) == 0:
        return format_org_comment(comment, level)
    else:
        message = format_org_comment(comment, level)
        for comment in comment.replies:
            message += create_message(comment, level+1)
        return message

    
def format_org_comment(comment, level):
    comment_author = comment.author.name if hasattr(comment, 'author') and hasattr(comment.author, 'name') else "unknown"
    comment_timestamp = dt.fromtimestamp(float(comment.created_utc)).strftime("%Y-%m-%d %H:%M:%S")

    # transform reddit formatting for org rendering
    if comment.body:
        # replace * with - to transform subreddit bullet to org-bullets and not confuse with org-heading
        comment_body = "-" + comment.body[1:] if len(comment.body) > 0 and comment.body[0] == "*" else comment.body
        comment_body = comment_body.replace("\n*", "\n-")
        comment_body = "{}\n".format(comment_body)
    else:
        comment_body = ""

    message = '*' * level + "** {} :@{}:\n{}".format(comment_timestamp, comment_author, comment_body)
    return message


if __name__ == "__main__":
    # check arguments
    if len(sys.argv) < 4 or len(sys.argv) > 7:
        print("Incorrect Arguments! Correct Example:\n",
              "./subreddit <username> <password> <subredditname> [conversion_type(rst|md)]")
        sys.exit()

    # if conversion format argument passed
    if len(sys.argv) >= 5:
        # if specified import subreddit in rst format
        if sys.argv[4] == "rst":
            print("Importing in format: reStructuredText")
            import2rst(sys.argv[1], sys.argv[2], sys.argv[3])

        # elif specified import subreddit in md format
        elif sys.argv[4] == "md":
            print("Importing in format: Markdown")
            import2md(sys.argv[1], sys.argv[2], sys.argv[3])

        # elif specified import subreddit in org format
        elif sys.argv[4] == "org":
            print("Importing in format: Org-Mode")
            import2org(sys.argv[1], sys.argv[2], sys.argv[3], outfile=sys.argv[5], title=sys.argv[6])
            print("Transfor format: Org-Mode - HTML")
            check_output("emacs {}.org --batch --eval '(setq org-html-mathjax-template \"\")' --eval '(setq org-html-inline-images nil)' -f org-html-export-to-html --kill".format(sys.argv[5]), shell=True)

        elif sys.argv[4] == "panchayat":
            print("Importing in format: Panchayat")
            import2panchayat(sys.argv[1],
                             sys.argv[2],
                             sys.argv[3],
                             indb_path=sys.argv[5],
                             outdb_path=sys.argv[6])
            
        # else informing user of correct argument for rst, md, org or panchayat conversion
        else:
            print("Unknown conversion format")
            print("The last argument should be 'rst' for reStructured Text, 'md' for Markdown, 'org' for OrgMode, 'panchayat' for Panchayat")

    # else
    else:
        print("Defaulting to conversion format: Markdown(md)")
        import2md(sys.argv[1], sys.argv[2], sys.argv[3])
