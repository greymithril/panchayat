from panchayat.vdb import VDB, User, LinkPost, TextPost
import click
import yaml
import os
import sqlite3
import datetime


@click.command()
@click.option('--sql',
              'sqlfile',
              required=True,
              type=click.Path(exists=True),
              help='path to sql db file')
@click.option('--yaml',
              'yamlfile',
              type=click.Path(),
              help='path to output yaml file')
def cli(sqlfile, yamlfile):
    db = sqlite3.connect(sqlfile)
    db.row_factory = sqlite3.Row
    c = db.cursor()
    db_users = [
        dict(user) for user in c.execute(
            'SELECT id, username, password, token, email FROM user').fetchall(
            )
    ]
    db_posts = [
        dict(post) for post in c.execute(
            'SELECT p.id, p.title, p.body, p.created, u.username, p.parent, p.is_url FROM post p JOIN user u WHERE p.author_id == u.id ORDER BY p.id ASC '
        ).fetchall()
    ]
    db_votes = [
        dict(vote) for vote in c.execute(
            'SELECT v.vote, v.post_id, u.username FROM votes v JOIN user u WHERE v.user_id == u.id'
        ).fetchall()
    ]

    vdb = VDB()
    for db_user in db_users:
        vdb.users.append(
            User(username=db_user['username'],
                 password=db_user['password'],
                 token=db_user['token'],
                 email=db_user['email']))
    for db_post in db_posts:
        author = vdb.users.find(db_post['username'])
        parent = vdb.posts.find(
            db_post['parent']) if db_post['parent'] else None
        created = datetime.datetime.strptime(db_post['created'],
                                             '%Y-%m-%d %H:%M:%S')
        if db_post['is_url']:
            vdb.posts.insert(
                LinkPost(
                    author=author,
                    title=db_post['title'],
                    body=db_post['body'],
                    created=created,
                    parent=parent,
                    id=db_post['id'],
                ))
        else:
            vdb.posts.insert(
                TextPost(
                    author=author,
                    title=db_post['title'],
                    body=db_post['body'],
                    created=created,
                    parent=parent,
                    id=db_post['id'],
                ))
    for db_vote in db_votes:
        vote = db_vote['vote']
        user = vdb.users.find(db_vote['username'])
        post = vdb.posts.find(db_vote['post_id'])
        if vote == 1:
            post.upvote(user)
        elif vote == 0:
            post.downvote(user)
        else:
            raise ValueError("Unknown vote")

    if yamlfile is None:
        _, tail = os.path.split(sqlfile)
        pre, ext = os.path.splitext(tail)
        yamlfile = f'./{pre}.yaml'
    with open(yamlfile, 'w') as output:
        yaml.dump(vdb, output)

    vdb.outfile = yamlfile
    vdb.commit()


if __name__ == '__main__':
    cli()
