# Panchayat
A minimal reddit style forum application derived off [flask tutorial](http://flask.pocoo.org/docs/1.0/tutorial)

## Setup
```
git clone https://gitlab.com/haletic/panchayat
cd panchayat
mkdir instance
#copy db file with name panchayat.sqlite to instance folder
```

### Docker
```
sudo docker build -t panchayat_dev .
sudo docker run -d -p 127.0.0.1:5555:5000/tcp -v $(pwd)/instance:/app/instance panchayat_dev
```

### Native
```

python3.6 -m venv venv
. venv/bin/activate
pip install -r panchayat/requirements.txt
export FLASK_APP=panchayat
export FLASK_ENV=development

flask run --host=0.0.0.0 --port=5555
```
